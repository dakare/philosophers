package ua.edu.zntu.drawables;

import ua.edu.zntu.animate.Animation;
import ua.edu.zntu.player.OnPositionChangeListener;

import java.awt.*;

public interface Drawable extends OnPositionChangeListener
{

    void resize(int containerSize);

    void draw(Graphics2D graphics2D, int offsetTop, int offsetLeft);

    void addAnimation(Animation animation);

    void clearAnimations();
}
