package ua.edu.zntu.drawables;

import ua.edu.zntu.animate.Animation;
import ua.edu.zntu.animate.AnimationType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.util.ArrayList;

public class DishDrawable extends CircleDrawable
{
    public static final double SIZE = 0.08;
    private static final Image SOURCE;

    static
    {
        try
        {
            SOURCE = ImageIO.read(FlatwareDrawable.class.getClassLoader().getResourceAsStream("dish.png"));
        } catch (IOException e)
        {
            throw new RuntimeException("Failed to load flatware image", e);
        }
    }

    private final int index;
    private final double angle;
    private int size;
    private int x;
    private int y;
    private double scale;
    private java.util.List<Animation> animations = new ArrayList<>();
    private boolean visible;

    public DishDrawable(int index)
    {
        this.index = index;
        angle = Math.toRadians(360. * index / PhilosopherDrawable.PHILOSOPHERS_COUNT);
    }

    @Override
    public void resize(int containerSize)
    {
        size = (int) (containerSize * SIZE);
        scale = (double) size / Math.max(SOURCE.getWidth(null), SOURCE.getHeight(null));
        double deltaX = Math.sin(angle + Math.PI / 2) * size;
        double deltaY = Math.cos(angle + Math.PI / 2) * size;
        int offset = (int) (containerSize * (1 - PhilosopherDrawable.SIZE * 2) / 2);
        x = (int) (containerSize / 2 - Math.sin(angle) * (offset - size) + deltaX / 2);
        y = (int) (containerSize / 2 - Math.cos(angle) * (offset - size) + deltaY / 2);
    }

    @Override
    public void draw(Graphics2D graphics2D, int offsetTop, int offsetLeft)
    {
        if (visible)
        {
            AffineTransform transform = new AffineTransform();
            transform.setToTranslation(x + offsetLeft, y + offsetTop);
            transform.scale(scale, scale);
            transform.rotate(Math.PI - angle);
            graphics2D.drawImage(SOURCE, transform, null);
        }
    }

    @Override
    public void addAnimation(Animation animation)
    {
        if (animation.getType() != AnimationType.SHOW)
        {
            throw new IllegalArgumentException(animation.getType() + " animation is not supported by Dish animation");
        }
        animations.add(animation);
    }

    @Override
    public void clearAnimations()
    {
        animations.clear();
    }

    @Override
    public void onPositionChanged(int newPosition)
    {
        for (Animation animation : animations)
        {
            if (animation.isApplyable(newPosition))
            {
                visible = true;
                return;
            }
        }
        visible = false;
    }
}
