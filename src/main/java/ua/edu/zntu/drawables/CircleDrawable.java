package ua.edu.zntu.drawables;

import java.awt.*;

public abstract class CircleDrawable implements Drawable
{

    protected void drawCircle(Graphics2D graphics, int x, int y, int size, Color color)
    {
        graphics.setColor(color);
        graphics.fillOval(x, y, size, size);
        graphics.setColor(Color.black);
        graphics.drawOval(x, y, size, size);
    }
}
