package ua.edu.zntu.drawables;

import ua.edu.zntu.animate.Animation;
import ua.edu.zntu.animate.AnimationType;
import ua.edu.zntu.animate.ChangeNameAnimation;

import java.awt.*;
import java.util.ArrayList;

public class PhilosopherDrawable extends CircleDrawable
{
    public static final double SIZE = 0.17;
    public static final int PHILOSOPHERS_COUNT = 5;
    private static final Color COLOR = new Color(0xff, 0xfb, 0x07);

    private final int index;
    private final double angle;
    private int size;
    private int x;
    private int y;
    private java.util.List<Animation> animations = new ArrayList<>();
    private String text;

    public PhilosopherDrawable(int index)
    {
        this.index = index;
        angle = Math.toRadians(360. * index / PHILOSOPHERS_COUNT);
        text = "�" + (index + 1);
    }

    @Override
    public void resize(int containerSize)
    {
        size = (int) (containerSize * SIZE);
        x = (int) ((containerSize - Math.sin(angle) * (containerSize - size)) / 2);
        y = (int) ((containerSize - Math.cos(angle) * (containerSize - size)) / 2);
    }

    @Override
    public void draw(Graphics2D graphics2D, int offsetTop, int offsetLeft)
    {
        drawCircle(graphics2D, x + offsetLeft - size / 2, y + offsetTop - size / 2, size, COLOR);
        Font font = new Font("Courier New", 0, 18);
        graphics2D.setFont(font);
        FontMetrics fm = graphics2D.getFontMetrics();
        int width = (fm.stringWidth(text) * 2) + 4;
        graphics2D.drawString(text, x + offsetLeft - width / 4, y + offsetTop - fm.getHeight() / 2 + fm.getAscent());
    }

    @Override
    public void addAnimation(Animation animation)
    {
        if (animation.getType() != AnimationType.CHANGE_NAME)
        {
            throw new IllegalArgumentException(animation.getType() + " animation is not supported by Philosopher animation");
        }
        animations.add(animation);
    }

    @Override
    public void clearAnimations()
    {
        animations.clear();
    }

    @Override
    public void onPositionChanged(int newPosition)
    {
        for (Animation animation : animations)
        {
            if (animation.isApplyable(newPosition))
            {
                text = "�" + (index + 1) + " " + ((ChangeNameAnimation) animation).getName();
                return;
            }
        }
        text = "�" + (index + 1);
    }
}
