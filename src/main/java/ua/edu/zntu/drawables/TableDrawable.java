package ua.edu.zntu.drawables;

import ua.edu.zntu.animate.Animation;

import java.awt.*;

public class TableDrawable extends CircleDrawable
{
    private static final Color TABLE_COLOR = new Color(0xd0, 0xd0, 0xd0);
    private int size;
    private int offset;

    @Override
    public void resize(int containerSize)
    {
        size = (int) (containerSize * (1 - PhilosopherDrawable.SIZE));
        offset = (containerSize - size) / 2;
    }

    @Override
    public void draw(Graphics2D graphics2D, int offsetTop, int offsetLeft)
    {
        drawCircle(graphics2D, offset + offsetLeft, offset + offsetTop, size, TABLE_COLOR);
    }

    @Override
    public void addAnimation(Animation animation)
    {
        throw new UnsupportedOperationException("No animations for table");
    }

    @Override
    public void clearAnimations()
    {
        //Nothing to do
    }

    @Override
    public void onPositionChanged(int newPosition)
    {

    }
}
