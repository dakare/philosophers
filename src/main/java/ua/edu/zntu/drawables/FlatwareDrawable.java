package ua.edu.zntu.drawables;

import ua.edu.zntu.animate.Animation;
import ua.edu.zntu.animate.AnimationType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;

public class FlatwareDrawable implements Drawable
{
    private static final Image SOURCE;
    private static final float FLATWARE_SIZE = 0.2f;

    static
    {
        try
        {
            SOURCE = ImageIO.read(FlatwareDrawable.class.getClassLoader().getResourceAsStream("flatware.png"));
        } catch (IOException e)
        {
            throw new RuntimeException("Failed to load flatware image", e);
        }
    }

    private final double angle;
    private final double rotateAngle;
    private int x;
    private int y;
    private int size;
    private int offset;
    private double deltaY;
    private double deltaX;
    private double scale;
    private java.util.List<Animation> animations = new ArrayList<>();
    private double currentAngle;
    private double containerSize;
    private double animationScale;

    public FlatwareDrawable(int index)
    {
        currentAngle = angle = Math.toRadians(360. / PhilosopherDrawable.PHILOSOPHERS_COUNT / 2 + 360. * index / PhilosopherDrawable.PHILOSOPHERS_COUNT);
        rotateAngle = Math.toRadians(360. / PhilosopherDrawable.PHILOSOPHERS_COUNT / 2);
    }

    @Override
    public void resize(int containerSize)
    {
        this.containerSize = containerSize;
        size = (int) (containerSize * FLATWARE_SIZE);
        offset = (int) (containerSize * (1 - PhilosopherDrawable.SIZE * 2) / 2);
        updatePosition();
    }

    private void updatePosition()
    {
        scale = (double) size / SOURCE.getHeight(null);
        int width = SOURCE.getWidth(null);
        deltaX = Math.sin(currentAngle + Math.PI / 2) * width * scale;
        deltaY = Math.cos(currentAngle + Math.PI / 2) * width * scale;
        x = (int) (containerSize / 2 - Math.sin(currentAngle) * (offset - size) + deltaX / 2);
        y = (int) (containerSize / 2 - Math.cos(currentAngle) * (offset - size) + deltaY / 2);
    }

    @Override
    public void draw(Graphics2D graphics2D, int offsetTop, int offsetLeft)
    {
        AffineTransform transform = new AffineTransform();
        transform.setToTranslation(x + offsetLeft, y + offsetTop);
        transform.scale(scale, scale);
        transform.rotate(Math.PI - angle - Math.PI / 8 * animationScale);
        graphics2D.drawImage(SOURCE, transform, null);
    }

    @Override
    public void addAnimation(Animation animation)
    {
        if (!EnumSet.of(AnimationType.MOVE_LEFT, AnimationType.MOVE_RIGHT, AnimationType.STAY_LEFT, AnimationType.STAY_RIGHT).contains(animation.getType()))
        {
            throw new IllegalArgumentException("Animation of type " + animation.getType() + " is not supported by flatware drawable.");
        }
        animations.add(animation);
    }

    @Override
    public void clearAnimations()
    {
        animations.clear();
    }

    @Override
    public void onPositionChanged(int newPosition)
    {
        for (Animation animation : animations)
        {
            if (animation.isApplyable(newPosition))
            {
                switch (animation.getType())
                {
                    case MOVE_LEFT:
                        animationScale = -animation.getScale(newPosition);
                        currentAngle = angle + rotateAngle * animationScale;
                        updatePosition();
                        return;
                    case MOVE_RIGHT:
                        animationScale = animation.getScale(newPosition);
                        currentAngle = angle + rotateAngle * animationScale;
                        updatePosition();
                        return;
                    case STAY_LEFT:
                        animationScale = -1;
                        currentAngle = angle + rotateAngle * animationScale;
                        updatePosition();
                        return;
                    case STAY_RIGHT:
                        animationScale = 1;
                        currentAngle = angle + rotateAngle * animationScale;
                        updatePosition();
                        return;
                    default:
                        throw new AssertionError("This should not happen");
                }
            }
        }
        animationScale = 0;
        currentAngle = angle;
        updatePosition();
    }
}
