package ua.edu.zntu.animate;

public enum AnimationType
{
    MOVE_LEFT,
    MOVE_RIGHT,
    STAY_LEFT,
    STAY_RIGHT,
    SHOW,
    CHANGE_NAME;
}
