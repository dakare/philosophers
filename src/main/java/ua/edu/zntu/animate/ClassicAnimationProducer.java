package ua.edu.zntu.animate;

import ua.edu.zntu.drawables.Drawable;
import ua.edu.zntu.drawables.PhilosopherDrawable;
import ua.edu.zntu.player.OnPositionChangeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ClassicAnimationProducer extends AbstractAnimationProducer
{

    public ClassicAnimationProducer(List<Drawable> philosophers, List<Drawable> flatwares)
    {
        super(philosophers, flatwares, null);
        animate(1, 2000);
        animate(2, 7000);
        animate(3, 12000);
        for (int i = 0; i < philosophers.size(); i++)
        {
            takeLeft(i, 17000 + i * 100, 60000);
            waitLeft(i, 17000 + i * 100 + getEatCycle() / 8, 60000);
        }
    }

    private void animate(int index, int startTime)
    {
        Drawable philosopher = philosophers.get(index);
        Drawable left = flatwares.get(getLeft(index));
        Drawable right = flatwares.get(getRight(index));
        philosopher.addAnimation(new ChangeNameAnimation("���� ����� ������").withPeriod(startTime, startTime + getEatCycle() / 8));
        philosopher.addAnimation(new ChangeNameAnimation("���� ������ ������").withPeriod(startTime + getEatCycle() / 8, startTime + getEatCycle() / 4));
        philosopher.addAnimation(new ChangeNameAnimation("��").withPeriod(startTime + getEatCycle() / 4, startTime + getEatCycle() * 3 / 4));
        philosopher.addAnimation(new ChangeNameAnimation("����� ����� ������").withPeriod(startTime + getEatCycle() * 3 / 4, startTime + getEatCycle() * 7 / 8));
        philosopher.addAnimation(new ChangeNameAnimation("����� ������ ������").withPeriod(startTime + getEatCycle() * 7 / 8, startTime + getEatCycle()));
        left.addAnimation(new Animation(AnimationType.MOVE_RIGHT).withPeriod(startTime, startTime + getEatCycle() / 8));
        left.addAnimation(new Animation(AnimationType.MOVE_RIGHT).withPeriod(startTime + getEatCycle() * 3 / 4, startTime + getEatCycle() * 7 / 8).setBackward(true));
        left.addAnimation(new Animation(AnimationType.STAY_RIGHT).withPeriod(startTime + getEatCycle() / 8, startTime + getEatCycle() * 3 / 4));
        right.addAnimation(new Animation(AnimationType.STAY_LEFT).withPeriod(startTime + getEatCycle() / 4, startTime + getEatCycle() * 7 / 8));
        right.addAnimation(new Animation(AnimationType.MOVE_LEFT).withPeriod(startTime + getEatCycle() / 8, startTime + getEatCycle() / 4));
        right.addAnimation(new Animation(AnimationType.MOVE_LEFT).withPeriod(startTime + getEatCycle() * 7 / 8, startTime + getEatCycle()).setBackward(true));
    }


    @Override
    public void onPositionChanged(int newPosition)
    {
        //Nothing to do
    }
}
