package ua.edu.zntu.animate;

import ua.edu.zntu.drawables.Drawable;
import ua.edu.zntu.drawables.PhilosopherDrawable;
import ua.edu.zntu.player.OnPositionChangeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class AbstractAnimationProducer implements OnPositionChangeListener
{

    protected final List<Drawable> philosophers;
    protected final List<Drawable> flatwares;
    protected final List<Drawable> dishes;

    public AbstractAnimationProducer(List<Drawable> philosophers, List<Drawable> flatwares, List<Drawable> dishes)
    {
        this.philosophers = philosophers;
        this.flatwares = flatwares;
        this.dishes = dishes;
    }

    protected int getLeft(int index)
    {
        return (index + PhilosopherDrawable.PHILOSOPHERS_COUNT - 1) % PhilosopherDrawable.PHILOSOPHERS_COUNT;
    }

    protected int getRight(int index)
    {
        return index % PhilosopherDrawable.PHILOSOPHERS_COUNT;
    }

    protected int getEatCycle()
    {
        return 4000;
    }

    protected void takeLeft(int index, int from, int to)
    {
        philosophers.get(index).addAnimation(new ChangeNameAnimation("���� ����� ������").withPeriod(from, from + getEatCycle() / 8));
        Drawable left = flatwares.get(getLeft(index));
        left.addAnimation(new Animation(AnimationType.MOVE_RIGHT).withPeriod(from, from + getEatCycle() / 8));
        left.addAnimation(new Animation(AnimationType.STAY_RIGHT).withPeriod(from + getEatCycle() / 8, to));
    }

    protected void waitLeft(int index, int from, int to)
    {
        philosophers.get(index).addAnimation(new ChangeNameAnimation("��� ����� ������").withPeriod(from, to));
    }

    protected void waitRight(int index, int from, int to)
    {
        philosophers.get(index).addAnimation(new ChangeNameAnimation("��� ������ ������").withPeriod(from, to));
    }
}
