package ua.edu.zntu.animate;

public class Animation
{
    private final AnimationType type;
    private int startTime;
    private int endTime;
    private boolean backward;

    public Animation(AnimationType type)
    {
        this.type = type;
    }

    public AnimationType getType()
    {
        return type;
    }

    public Animation withPeriod(int startTime, int endTime)
    {
        this.startTime = startTime;
        this.endTime = endTime;
        return this;
    }

    public Animation setBackward(boolean backward)
    {
        this.backward = backward;
        return this;
    }

    public boolean isApplyable(int time)
    {
        if (startTime == 0 && endTime == 0)
        {
            System.out.println("Waning: animation has no start and end period");
            return false;
        }
        return startTime < time && endTime >= time;
    }

    public float getScale(int currentTime)
    {
        float scale = (float)(currentTime - startTime) / (endTime - startTime);
        return backward ? 1 - scale : scale;
    }
}
