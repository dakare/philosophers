package ua.edu.zntu.animate;

import ua.edu.zntu.drawables.Drawable;
import ua.edu.zntu.drawables.PhilosopherDrawable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

public class ServantAnimationProducer extends AbstractAnimationProducer
{

    private final List<StateUpdater> updaters = new ArrayList<>(PhilosopherDrawable.PHILOSOPHERS_COUNT);
    private final State[] states = new State[PhilosopherDrawable.PHILOSOPHERS_COUNT];
    private final boolean[] inUse = new boolean[PhilosopherDrawable.PHILOSOPHERS_COUNT];
    private final Random random = new Random();
    private int eatingNow = 0;

    public ServantAnimationProducer(List<Drawable> philosophers, List<Drawable> flatwares, List<Drawable> dishes)
    {
        super(philosophers, flatwares, dishes);
        for (int i = 0; i < states.length; i++)
        {
            states[i] = State.THINKING;
            updaters.add(new StateUpdater(random.nextInt(3000), i, State.WAITING_DISH));
        }
    }

    @Override
    public void onPositionChanged(int newPosition)
    {
        List<StateUpdater> finished = new ArrayList<>(updaters.size());
        List<StateUpdater> newUpdaters = new ArrayList<>(updaters.size());
        for (StateUpdater updater : updaters)
        {
            if (newPosition >= updater.time)
            {
                ifExists(updateState(updater), newUpdaters::add);
                finished.add(updater);
            }
        }
        updaters.removeAll(finished);
        updaters.addAll(newUpdaters);
        for (int i = 0; i < states.length; i++)
        {
            State state = states[i];
            if (state == State.WAITING_DISH)
            {
                checkDishFlatware(i, newPosition);
            } else if (state == State.WAIT_LEFT)
            {
                ifExists(checkLeftFlatware(i, newPosition), updaters::add);
            } else if (state == State.WAIT_RIGHT)
            {
                ifExists(checkRightFlatware(i, newPosition), updaters::add);
            }
        }
    }

    private void ifExists(StateUpdater updater, Consumer<StateUpdater> callback)
    {
        if (updater != null)
        {
            callback.accept(updater);
        }
    }

    private StateUpdater updateState(StateUpdater updater)
    {
        states[updater.index] = updater.state;
        switch (updater.state)
        {
            case THINKING:
                eatingNow--;
                dishes.get(updater.index).clearAnimations();
                inUse[getLeft(updater.index)] = false;
                inUse[getRight(updater.index)] = false;
                return new StateUpdater(updater.time + random.nextInt(3000), updater.index, State.WAITING_DISH);
            case WAITING_DISH:
                waitDish(updater.index, updater.time);
                checkDishFlatware(updater.index, updater.time);
                break;
            case WAIT_LEFT:
                waitLeft(updater.index, updater.time, Integer.MAX_VALUE);
                return checkLeftFlatware(updater.index, updater.time);
            case TAKE_LEFT:
                //Nothing to do
                break;
            case WAIT_RIGHT:
                waitRight(updater.index, updater.time, Integer.MAX_VALUE);
                return checkRightFlatware(updater.index, updater.time);
            case TAKE_RIGHT:
                //Nothing to do
                break;
            default:
                throw new UnsupportedOperationException("Invalid state");
        }
        return null;
    }

    private void waitDish(int index, int from)
    {
        philosophers.get(index).addAnimation(new ChangeNameAnimation("��� �����").withPeriod(from, Integer.MAX_VALUE));
    }

    private void eat(int index, int startTime)
    {
        Drawable philosopher = philosophers.get(index);
        philosopher.clearAnimations();
        Drawable left = flatwares.get(getLeft(index));
        Drawable right = flatwares.get(getRight(index));
        left.clearAnimations();
        philosopher.addAnimation(new ChangeNameAnimation("���� ������ ������").withPeriod(startTime, startTime + getEatCycle() / 8));
        philosopher.addAnimation(new ChangeNameAnimation("��").withPeriod(startTime + getEatCycle() / 8, startTime +  getEatCycle() * 5 / 8));
        philosopher.addAnimation(new ChangeNameAnimation("����� ����� ������").withPeriod(startTime +  getEatCycle() * 5 / 8, startTime + getEatCycle() * 6 / 8));
        philosopher.addAnimation(new ChangeNameAnimation("����� ������ ������").withPeriod(startTime + getEatCycle() * 6 / 8, startTime +  getEatCycle() * 7 / 8));
        left.addAnimation(new Animation(AnimationType.MOVE_RIGHT).withPeriod(startTime +  getEatCycle() * 5 / 8, startTime + getEatCycle() * 6 / 8).setBackward(true));
        left.addAnimation(new Animation(AnimationType.STAY_RIGHT).withPeriod(startTime - 40, startTime + getEatCycle() * 5 / 8));
        right.addAnimation(new Animation(AnimationType.STAY_LEFT).withPeriod(startTime + getEatCycle() / 8, startTime + getEatCycle() * 6 / 8));
        right.addAnimation(new Animation(AnimationType.MOVE_LEFT).withPeriod(startTime, startTime + getEatCycle() / 8));
        right.addAnimation(new Animation(AnimationType.MOVE_LEFT).withPeriod(startTime + getEatCycle() * 6 / 8, startTime +  getEatCycle() * 7 / 8).setBackward(true));
    }

    private void checkDishFlatware(int index, int time)
    {
        if (eatingNow + 1 < PhilosopherDrawable.PHILOSOPHERS_COUNT)
        {
            showDish(index, time);
            eatingNow++;
            waitLeft(index, time, Integer.MAX_VALUE);
            states[index] = State.WAIT_LEFT;
        }
    }

    protected void showDish(int index, int from)
    {
        dishes.get(index).addAnimation(new Animation(AnimationType.SHOW).withPeriod(from, Integer.MAX_VALUE));
    }

    private StateUpdater checkLeftFlatware(int index, int time)
    {
        if (!inUse[getLeft(index)])
        {
            inUse[getLeft(index)] = true;
            takeLeft(index, time, Integer.MAX_VALUE);
            states[index] = State.TAKE_LEFT;
            return new StateUpdater(time + getEatCycle() / 8, index, State.WAIT_RIGHT);
        }
        return null;
    }

    private StateUpdater checkRightFlatware(int index, int time)
    {
        if (!inUse[getRight(index)])
        {
            inUse[getRight(index)] = true;
            eat(index, time);
            states[index] = State.TAKE_RIGHT;
            return new StateUpdater(time + getEatCycle() * 7 / 8, index, State.THINKING);
        }
        return null;
    }

    @Override
    protected void takeLeft(int index, int from, int to)
    {
        flatwares.get(getLeft(index)).clearAnimations();
        super.takeLeft(index, from, to);
    }

    @Override
    protected void waitLeft(int index, int from, int to)
    {
        philosophers.get(index).clearAnimations();
        super.waitLeft(index, from, to);
    }

    private static class StateUpdater
    {
        public int time;
        public int index;
        public State state;

        public StateUpdater(int time, int index, State state)
        {
            this.time = time;
            this.index = index;
            this.state = state;
        }
    }

    private enum State
    {
        THINKING,
        WAITING_DISH,
        WAIT_LEFT,
        TAKE_LEFT,
        WAIT_RIGHT,
        TAKE_RIGHT;
    }
}
