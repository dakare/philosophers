package ua.edu.zntu.animate;

public class ChangeNameAnimation extends Animation
{
    private final String name;

    public ChangeNameAnimation(String name)
    {
        super(AnimationType.CHANGE_NAME);
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
