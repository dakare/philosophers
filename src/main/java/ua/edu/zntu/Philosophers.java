package ua.edu.zntu;

import ua.edu.zntu.view.*;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

public class Philosophers extends JFrame {

    private static Philosophers INSTANCE;
    private JPanel content;
    private Views currentView;
    private MenuPanel menuPanel = new MenuPanel();
    private DescriptionPanel descriptionPanel = new DescriptionPanel();
    private PlayerPanel classicPanel = new ClassicSolutionPanel();
    private PlayerPanel servantSolutionPanel = new ServantSolutionPanel();

    public Philosophers() {
        super("Philosophers");
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        if (gd.isFullScreenSupported()) {
            gd.setFullScreenWindow(this);
        } else {
            System.err.println("Full screen not supported");
            setSize(1024, 768);
        }
        content = new JPanel(new CardLayout());
        content.add(menuPanel, Views.MENU.name());
        content.add(descriptionPanel, Views.DESCRIPTION.name());
        content.add(classicPanel, Views.CLASSIC.name());
        content.add(servantSolutionPanel, Views.SERVANT.name());
        getContentPane().add(content);
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "backToMenu");
        getRootPane().getActionMap().put("backToMenu", new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (INSTANCE.currentView == Views.MENU)
                {
                    dispose();
                } else
                {
                    Philosophers.setView(Views.MENU);
                }
            }
        });
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void setView(Views view)
    {
        System.out.println("Set view " + view);
        CardLayout layout = (CardLayout) INSTANCE.content.getLayout();
        layout.show(INSTANCE.content, view.name());
        INSTANCE.currentView = view;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        INSTANCE = new Philosophers();
        SwingUtilities.invokeLater(() -> {
            System.out.println("Start");
            INSTANCE.setView(Views.MENU);
            INSTANCE.setVisible(true);
        });
    }
}