package ua.edu.zntu.player;

@FunctionalInterface
public interface OnPositionChangeListener
{
    void onPositionChanged(int newPosition);
}
