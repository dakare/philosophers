package ua.edu.zntu.player;

import java.util.ArrayList;
import java.util.List;

public class MediaPlayer implements Runnable
{
    private PlayerState state = PlayerState.STOP;
    private int duration;
    private int position;
    private long startTime;
    private List<OnPositionChangeListener> listeners = new ArrayList<>();
    private Thread thread = new Thread(() -> {});

    public void play()
    {
        if (isPlaying())
        {
            return;
        }
        startTime = System.currentTimeMillis();
        state = PlayerState.PLAY;
        thread = new Thread(this);
        thread.start();
    }

    public void pause()
    {
        if (!isPlaying())
        {
            return;
        }
        state = PlayerState.PAUSE;
        updatePosition();
        thread.interrupt();
    }

    private void updatePosition()
    {
        long current = System.currentTimeMillis();
        position += (current - startTime);
        startTime = current;
        if (position > duration)
        {
            stop();
        }
    }

    public void stop()
    {
        if (state == PlayerState.STOP)
        {
            return;
        }
        state = PlayerState.STOP;
        position = 0;
        thread.interrupt();
        notifyListeners();
    }

    public void forward()
    {
        position += 2000;
        if (position > duration)
        {
            stop();
        }
        notifyListeners();
    }

    public void updatePosition(int position)
    {
        if (Math.abs(this.position - position) > 1000)
        {
            thread.interrupt();
            state = PlayerState.PAUSE;
            this.position = position;
            notifyListeners();
        }
    }

    public void backward()
    {
        if (position == 0)
        {
            return;
        }
        position -= 2000;
        position = Math.max(0, position);
        notifyListeners();
    }

    private void notifyListeners()
    {
        listeners.forEach(ref -> ref.onPositionChanged(position));
    }

    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    public int getDuration()
    {
        return duration;
    }

    public int getPosition()
    {
        return position;
    }

    private boolean isPlaying()
    {
        return state == PlayerState.PLAY;
    }

    public void addListener(OnPositionChangeListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void run()
    {
        while(!Thread.interrupted())
        {
            updatePosition();
            notifyListeners();
            try
            {
                Thread.sleep(20);
            } catch (InterruptedException e)
            {
                break;
            }

        }
    }
}
