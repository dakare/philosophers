package ua.edu.zntu.player;

public enum PlayerState
{
    PLAY,
    PAUSE,
    STOP;
}
