package ua.edu.zntu.view;

import ua.edu.zntu.Philosophers;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

public class MenuPanel extends JPanel
{
    private Image image;
    {
        try
        {
            image = ImageIO.read(getClass().getClassLoader().getResourceAsStream("philosophers.jpg"));

        } catch (IOException e)
        {
            System.out.println("Failed to load image");
        }
        setLayout(new GridBagLayout());
        JPanel centerWrap = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridy = 0;
        constraints.ipady = 40;
        JLabel label = new JLabel("������ �� ��������� ���������");
        label.setFont(getTitleFont());
        centerWrap.add(label, constraints);
        constraints = new GridBagConstraints();
        constraints.gridy = 1;
        constraints.ipady = 10;
        constraints.insets.bottom = 20;
        constraints.insets.top = 80;
        centerWrap.add(createMenuButton("���������� ������", (e) -> Philosophers.setView(Views.DESCRIPTION)), constraints);
        constraints.insets.top = 0;
        constraints.gridy = 2;
        centerWrap.add(createMenuButton("������������ �������", e -> Philosophers.setView(Views.CLASSIC)), constraints);
        constraints.gridy = 3;
        centerWrap.add(createMenuButton("������� �� ������", e -> Philosophers.setView(Views.SERVANT)), constraints);
        centerWrap.setBackground(new Color(0, 0, 0, 0));
        add(centerWrap);
        setBackground(new Color(0, 0, 0, 0));
        System.out.println("Menu panel created");
    }

    private JButton createMenuButton(String label, ActionListener actionListener)
    {
        JButton button = new JButton(label);
        button.setPreferredSize(new Dimension(200, 40));
        button.setBorder(new EmptyBorder(10, 10, 10, 10));
        button.addActionListener(actionListener);
        return button;
    }

    private Font getTitleFont()
    {
        try
        {
            Font font = Font.createFont(Font.TRUETYPE_FONT, getClass().getClassLoader().getResourceAsStream("title.ttf"));
            return font.deriveFont(50f);
        } catch (Exception e)
        {
            e.printStackTrace();
            return new Font("Courier New", 0, 30);
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        super.paintComponent(g);

    }
}
