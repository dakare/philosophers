package ua.edu.zntu.view;

import ua.edu.zntu.player.MediaPlayer;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ControlPanel extends JPanel implements ChangeListener
{
    private MediaPlayer player;
    private JSlider slider;
    private JLabel currentTime;

    public ControlPanel(MediaPlayer player)
    {
        this.player = player;
        player.addListener((newPosition) -> {
            slider.setValue((int) (newPosition * 1000. / player.getDuration()));
            currentTime.setText(formatTime(newPosition));
        });
        initUI();
    }

    private void initUI()
    {
        setLayout(new GridBagLayout());
        add(createButton(("play.png"), e -> player.play()), new GridBagConstraints());
        add(createButton(("pause.png"), e -> player.pause()), new GridBagConstraints()
        {{
                gridx = 1;
            }});
        add(createButton(("stop.png"), e -> player.stop()), new GridBagConstraints(){{gridx = 2;}});
        add(createButton(("forward.png"), e -> player.forward()), new GridBagConstraints(){{gridx = 3;}});
        add(createButton(("backward.png"), e -> player.backward()), new GridBagConstraints()
        {{
                gridx = 4;
            }});
        slider = new JSlider(0, 1000, 0);
        slider.setPreferredSize(new Dimension(500, 50));
        slider.addChangeListener(this);
        add(slider, new GridBagConstraints()
        {{
                gridy = 1;
                gridwidth = 5;
            }});
        currentTime = new JLabel(formatTime(player.getPosition()));
        add(currentTime, new GridBagConstraints()
        {{
                gridy = 2;
                anchor = WEST;
            }});
        add(new JLabel(formatTime(player.getDuration())), new GridBagConstraints()
        {{
                gridy = 2;
                gridx = 4;
                anchor = EAST;
            }});
    }

    private String formatTime(int time)
    {
        int seconds = time / 1000 % 60;
        int minutes = time / 1000 / 60;
        return String.format("%02d:%02d", minutes, seconds);
    }

    private JButton createButton(String icon, ActionListener callback)
    {
        JButton button = new JButton(getIcon(icon));
        button.addActionListener(callback);
        button.setPreferredSize(new Dimension(100, 50));
        return button;
    }

    private Icon getIcon(String name)
    {
        try
        {
            return new ImageIcon(ImageIO.read(getClass().getClassLoader().getResourceAsStream(name)));
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stateChanged(ChangeEvent e)
    {
        player.updatePosition(slider.getValue() * player.getDuration() / 1000);
    }
}
