package ua.edu.zntu.view;

import ua.edu.zntu.animate.*;
import ua.edu.zntu.drawables.Drawable;
import ua.edu.zntu.drawables.FlatwareDrawable;
import ua.edu.zntu.drawables.PhilosopherDrawable;
import ua.edu.zntu.drawables.TableDrawable;
import ua.edu.zntu.player.MediaPlayer;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class ClassicSolutionPanel extends PlayerPanel
{
    private TableDrawer content;
    private AbstractAnimationProducer producer;

    @Override
    protected JPanel getControlPanel(MediaPlayer mediaPlayer)
    {
        return new ControlPanel(mediaPlayer);
    }

    @Override
    protected String getDescription()
    {
        return "������������ ������ ��������� ��������:<br>" +
                "<ul>" +
                "<li>����������</li>" +
                "<li>����� ����� �����</li>" +
                "<li>����� ������ �����</li>" +
                "<li>���</li>" +
                "<li>������ ������ �����</li>" +
                "<li>������ ����� �����</li>" +
                "<li> ����������</li>" +
                "</ul>";
    }

    @Override
    protected JPanel getContent()
    {
        java.util.List<Drawable> philosophers;
        java.util.List<Drawable> flatwares;
        philosophers = new ArrayList<>(PhilosopherDrawable.PHILOSOPHERS_COUNT);
        flatwares = new ArrayList<>(PhilosopherDrawable.PHILOSOPHERS_COUNT);
        content = new TableDrawer();
        content.addDrawable(new TableDrawable());
        for (int i = 0; i < PhilosopherDrawable.PHILOSOPHERS_COUNT; i++)
        {
            philosophers.add(new PhilosopherDrawable(i));
            flatwares.add(new FlatwareDrawable(i));
        }
        philosophers.forEach(content::addDrawable);
        flatwares.forEach(content::addDrawable);
        producer = new ClassicAnimationProducer(philosophers, flatwares);
        return content;
    }

    @Override
    protected int getDuration()
    {
        return 60000;
    }

    @Override
    public void onPositionChanged(int newPosition)
    {
        producer.onPositionChanged(newPosition);
        content.updatePosition(newPosition);
    }
}
