package ua.edu.zntu.view;

import ua.edu.zntu.drawables.Drawable;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class TableDrawer extends JPanel
{
    private int offsetTop;
    private int offsetLeft;
    private int size;
    private java.util.List<Drawable> drawables = new ArrayList<>();

    @Override
    public void setSize(int width, int height)
    {
        super.setSize(width, height);
        size = Math.min(width, height);
        drawables.forEach(item -> item.resize(size));
        offsetTop = (height - size) / 2;
        offsetLeft = (width - size) / 2;
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawables.forEach(item -> item.draw(graphics, offsetTop, offsetLeft));
        graphics.dispose();
    }

    public void addDrawable(Drawable drawable)
    {
        drawables.add(drawable);
    }

    public void updatePosition(int newPosition)
    {
        drawables.forEach(drawable -> drawable.onPositionChanged(newPosition));
        repaint();
        revalidate();
    }
}
