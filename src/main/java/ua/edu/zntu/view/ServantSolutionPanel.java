package ua.edu.zntu.view;

import ua.edu.zntu.animate.*;
import ua.edu.zntu.drawables.*;
import ua.edu.zntu.player.MediaPlayer;

import javax.swing.*;
import java.util.ArrayList;

public class ServantSolutionPanel extends PlayerPanel
{

    private TableDrawer content;
    private AbstractAnimationProducer producer;

    @Override
    protected JPanel getControlPanel(MediaPlayer mediaPlayer)
    {
        return new ServantControlPanel(mediaPlayer);
    }

    @Override
    protected String getDescription()
    {
        return "������ ������� ���������� ����� ��� ������� � �������� (���)." +
                "</p><p style='text-indent: 20px'> ����� ��� ��� ������ ������ �������� ����� ����� � ����, � �� ������, " +
                "��� � ������������ ��� �� ����� " + (PhilosopherDrawable.PHILOSOPHERS_COUNT - 1) + " ���������." ;
    }

    @Override
    protected JPanel getContent()
    {
        java.util.List<Drawable> philosophers;
        java.util.List<Drawable> flatwares;
        java.util.List<Drawable> dishes;
        philosophers = new ArrayList<>(PhilosopherDrawable.PHILOSOPHERS_COUNT);
        flatwares = new ArrayList<>(PhilosopherDrawable.PHILOSOPHERS_COUNT);
        dishes = new ArrayList<>(PhilosopherDrawable.PHILOSOPHERS_COUNT);
        content = new TableDrawer();
        content.addDrawable(new TableDrawable());
        for (int i = 0; i < PhilosopherDrawable.PHILOSOPHERS_COUNT; i++)
        {
            philosophers.add(new PhilosopherDrawable(i));
            flatwares.add(new FlatwareDrawable(i));
            dishes.add(new DishDrawable(i));
        }
        philosophers.forEach(content::addDrawable);
        flatwares.forEach(content::addDrawable);
        dishes.forEach(content::addDrawable);
        producer = new ServantAnimationProducer(philosophers, flatwares, dishes);
        return content;
    }

    @Override
    protected int getDuration()
    {
        return Integer.MAX_VALUE;
    }

    @Override
    public void onPositionChanged(int newPosition)
    {
        producer.onPositionChanged(newPosition);
        content.updatePosition(newPosition);
    }
}
