package ua.edu.zntu.view;

import ua.edu.zntu.player.MediaPlayer;
import ua.edu.zntu.player.OnPositionChangeListener;

import javax.swing.*;
import java.awt.*;

public abstract class PlayerPanel extends JPanel implements OnPositionChangeListener
{
    public static final int DESCRIPTION_WIDTH = 250;
    private static final int CONTROL_HEIGHT = 200;
    private static final int BOUND = 30;
    private JPanel description;
    private JPanel controlPanel;
    private JPanel content;
    private int width;
    private int height;
    private MediaPlayer mediaPlayer;

    {
        setLayout(null);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDuration(getDuration());
        mediaPlayer.addListener(this);
        content = getContent();
        add(content);
        controlPanel = getControlPanel(mediaPlayer);
        add(controlPanel);
        description = new JPanel(new GridBagLayout());
        JLabel descriptionText = new JLabel(new StringBuilder("<html><p width=").append(DESCRIPTION_WIDTH)
                .append(" style='text-indent: 20px'>").append(getDescription()).append("</p></html>").toString());
        descriptionText.setFont(new Font("Times New Roman", 0, 20));
        description.add(descriptionText);
        add(description);
    }

    protected abstract JPanel getControlPanel(MediaPlayer mediaPlayer);

    protected abstract String getDescription();

    protected abstract JPanel getContent();

    protected abstract int getDuration();

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if (width != getWidth() || height != getHeight())
        {
            width = getWidth();
            height = getHeight();
            description.setSize(new Dimension(DESCRIPTION_WIDTH, getHeight() - BOUND * 2));
            description.setLocation(Math.max(0, width - DESCRIPTION_WIDTH - BOUND), BOUND);
            controlPanel.setSize(new Dimension(getWidth() - DESCRIPTION_WIDTH - 2 * BOUND, CONTROL_HEIGHT));
            controlPanel.setLocation(BOUND, Math.max(0, height - CONTROL_HEIGHT - BOUND));
            content.setSize(new Dimension(width - DESCRIPTION_WIDTH - BOUND * 2, height - CONTROL_HEIGHT - BOUND * 2));
            content.setLocation(BOUND, BOUND);
            repaint();
            revalidate();
        }
    }
}
