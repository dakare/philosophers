package ua.edu.zntu.view;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class DescriptionPanel extends JPanel
{
    private JLabel header;
    private JLabel text;
    private JScrollPane scroll;
    private int width = -1;
    private int height = -1;
    {
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new GridBagLayout());
        header = new JLabel();
        header.setFont(new Font("Times New Roman", 0, 24));
        contentPanel.add(header, new GridBagConstraints(){{this.insets.top = 20;}});
        try(InputStream image = getClass().getClassLoader().getResourceAsStream("philosophers_preview.png"))
        {
            BufferedImage rawImage = ImageIO.read(image);
            JLabel picLabel = new JLabel(new ImageIcon(rawImage.getScaledInstance(400, 400 * rawImage.getHeight() / rawImage.getWidth(), 0), "���. 1. ����� ������"));
            GridBagConstraints constraints = new GridBagConstraints();
            constraints.gridy = 1;
            constraints.insets.top = 15;
            contentPanel.add(picLabel, constraints);
        } catch (IOException e)
        {
            throw new RuntimeException("Failed to load Philosophers picture", e);
        }
        text = new JLabel();
        text.setFont(new Font("Times New Roman", 0, 24));
        contentPanel.add(text, new GridBagConstraints(){{gridy = 2;}});
        contentPanel.add(new Label(), new GridBagConstraints()
        {{
                weighty = 1;
                gridy = 9;
            }});
        scroll = new JScrollPane(contentPanel);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(scroll);
        scroll.setBorder(new EmptyBorder(0, 0, 0, 0));
        invalidateText();
        System.out.println("Description panel created");
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        invalidateText();
    }

    private void invalidateText()
    {
        System.out.println("Description resize from " + width + "," + height + " to " + getWidth() + "," + getHeight());
        header.setText(new StringBuilder("<html><p width=").append(width - 60).append(" style='text-indent: 20px'>���������� ������������ ������ �� ��������� ��������� � ������������ " +
                "������������ �. ���������. �� ������� ������ ����������� ���� �������, �� ������ �� " +
                "������� ����� ������� (�1-�5).</p></html>").toString());
        text.setText(new StringBuilder("<html><div width=").append(width - 60).append("><p style='text-indent: 20px; margin-top: 15px;'>� ������ ����� ��������� ����� � ����������. �� ����� ����� ���� ����� (B1-B5), " +
                "������ �� ������� ��������� ����� ����� ��������� ���������. ������ ������� ����� " +
                "���������� � ���� ����������: ���������� ��� ���� ��������. ��� ����, ����� ������ ����, " +
                "�������� ���������� ��� �����: ���� � ������ ����, � ������ � �����. �������� ���, " +
                "������� ������ ����� �� ����� � �������� ���������� �� ��� ���, ���� ����� ��" +
                "�������������.</p><p style='text-indent: 20px; margin-top: 15px;'>� ���� ������ ������� ��� ������� ��������: �������� ������� � ��������� �������.</p><p>�������� ������� ����� �����, ����� ������ " +
                "����� � ������ �� �������� ������ �����. " +
                "����������� ���������� �������� ����� �� �����, �� ������ �� ��������. ����� " +
                "������������� �������� ��������������� �������� ������ � ������������ ���������, ��� " +
                "��� �� ������� �� ����� ��������������� ������ �������.</p><p style='text-indent: 20px; margin-top: 15px;'>" +
                "��������� ������� ���������, ����� �������� ������������ ������������� � " +
                "������������ ���������� �����, ��������, ���� ����� �����. ��� ���� ��������� " +
                "��������� ��������, ��� ��� ����� �� ��� �� ����� ������ ����, �� ���� ������ �����.</p></div></html>").toString());
        scroll.setPreferredSize(new Dimension(getWidth(), getHeight()));
        if (width != getWidth() || height != getHeight())
        {
            width = getWidth();
            height = getHeight();
            repaint();
            revalidate();
        }
    }
}
