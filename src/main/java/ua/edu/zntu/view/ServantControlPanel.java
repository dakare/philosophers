package ua.edu.zntu.view;

import ua.edu.zntu.player.MediaPlayer;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ServantControlPanel extends JPanel
{
    private MediaPlayer player;

    public ServantControlPanel(MediaPlayer player)
    {
        this.player = player;
        initUI();
    }

    private void initUI()
    {
        setLayout(new GridBagLayout());
        add(createButton(("play.png"), e -> player.play()), new GridBagConstraints());
        add(createButton(("pause.png"), e -> player.pause()), new GridBagConstraints()
        {{
                gridx = 1;
            }});
    }

    private JButton createButton(String icon, ActionListener callback)
    {
        JButton button = new JButton(getIcon(icon));
        button.addActionListener(callback);
        button.setPreferredSize(new Dimension(100, 50));
        return button;
    }

    private Icon getIcon(String name)
    {
        try
        {
            return new ImageIcon(ImageIO.read(getClass().getClassLoader().getResourceAsStream(name)));
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
